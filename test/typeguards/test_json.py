"""Tests for the json module."""

from collections.abc import Mapping, Sequence
import logging
from typing import Dict, List, Optional, NamedTuple, TypedDict, TypeVar

import pytest

from typeguards.json import Schema, is_json, is_json_schema


class TestIsJson:
    """Test suite for is_json function."""

    @staticmethod
    @pytest.mark.parametrize("value", (None, 1, "foo", {1: 1}, {"foo": str}, []))
    def test_fails_invalid_objects(value: object) -> None:
        """Failure cases."""
        assert not is_json(value)

    @staticmethod
    @pytest.mark.parametrize(
        "value",
        (
            {},
            {"foo": "bar"},
            {"foo": {}},
            {"foo": []},
            {
                "str": "s",
                "int": 1,
                "float": 1.1,
                "bool": True,
                "none": None,
                "dict": {"foo": "bar"},
                "list": ["str", {"foo": "bar"}],
            },
        ),
    )
    def test_passes_valid_objects(value: object) -> None:
        """Success cases."""
        assert is_json(value)

    @staticmethod
    @pytest.mark.parametrize(
        "value, exp_logs",
        (
            ("foo", ({"func": "is_json", "args": ("foo", False)},)),
            (
                {"foo": "bar"},
                ({"func": "is_json", "args": ({"foo": "bar"}, True)},),
            ),
            (
                {"foo": 1, "bar": 2},
                ({"func": "is_json", "args": ({"foo": 1, "bar": 2}, True)},),
            ),
            (
                {"foo": ["bar", 1]},
                ({"func": "is_json", "args": ({"foo": ["bar", 1]}, True)},),
            ),
            (
                {"foo": {"bar": 1}},
                (
                    {"func": "is_json", "args": ({"bar": 1}, True)},
                    {"func": "is_json", "args": ({"foo": {"bar": 1}}, True)},
                ),
            ),
        ),
    )
    def test_logging(
        value: object,
        exp_logs: Sequence[Mapping[str, object]],
        caplog: pytest.LogCaptureFixture,
    ) -> None:
        """Log messages."""
        caplog.set_level(logging.DEBUG)
        is_json(value)
        assert len(caplog.records) == len(exp_logs)
        for log_record, exp_log in zip(caplog.records, exp_logs):
            assert log_record.levelname == "DEBUG"
            assert log_record.msg == "\n\tvalue=%s\n\tres=%s"
            assert log_record.funcName == exp_log["func"]
            assert log_record.args == exp_log["args"]


class SimpleDict(TypedDict):
    """TypedDict with only `JSONSimpleValue` types."""

    str_key: str
    int_key: int
    bool_key: bool


class NestedDict(TypedDict):
    """Nested TypedDict."""

    str_key: str
    int_key: int
    bool_key: bool
    schema_key: SimpleDict
    optional_schema_key: Optional[SimpleDict]
    dict_key: Dict[str, object]
    dict_key_2: dict[str, object]
    dict_key_3: dict[str, int]
    dict_key_4: Dict[str, str | int]
    dict_key_5: dict[str, Optional[str | int]]


class ListDict(TypedDict):
    """TypedDict with `JSONListValue` types."""

    str_key: str
    int_key: int
    bool_key: bool
    list_key: List[int]
    list_key_2: List[str | int]
    list_key_3: List[Optional[str | int]]
    list_key_4: list[Optional[str | int]]
    list_key_5: List[SimpleDict]
    list_key_6: List[dict[str, str | int]]
    list_key_7: List[Dict[str, Optional[str | int | float | bool]]]
    list_key_8: List[Dict[str, object]]


class SimpleTuple(NamedTuple):
    """NamedTuple with only `JSONSimpleValue` types."""

    str_key: str
    int_key: int
    bool_key: bool


class NestedTuple(NamedTuple):
    """Nested NamedTuple."""

    str_key: str
    int_key: int
    bool_key: bool
    schema_key: SimpleDict
    optional_schema_key: Optional[SimpleDict]
    dict_key: Dict[str, object]
    dict_key_2: dict[str, object]
    dict_key_3: dict[str, int]
    dict_key_4: Dict[str, str | int]
    dict_key_5: dict[str, Optional[str | int]]


class ListTuple(NamedTuple):
    """NamedTuple with `JSONListValue` types."""

    str_key: str
    int_key: int
    bool_key: bool
    list_key: List[int]
    list_key_2: List[str | int]
    list_key_3: List[Optional[str | int]]
    list_key_4: list[Optional[str | int]]
    list_key_5: List[SimpleDict]
    list_key_6: List[dict[str, str | int]]
    list_key_7: List[Dict[str, Optional[str | int | float | bool]]]
    list_key_8: List[Dict[str, object]]


class TestIsJsonSchema:
    """Test suite for is_json_schema function."""

    @staticmethod
    @pytest.mark.parametrize(
        "value, schema",
        (
            ("foo", str),
            (1, int),
            ([1, 2], list),
            ([1, 2], List[int]),
            ({"foo": "bar"}, list),
            ({"foo": "bar"}, List),
            ({"foo": "bar"}, dict[str, int]),
            ({"foo": "bar"}, Dict[str, int]),
            ({"foo": ["bar", 1]}, dict[str, list[str]]),
            ({"foo": ["bar", 1]}, Dict[str, List[str]]),
            (1, int | list),
            ({"foo": "bar"}, int | list),
            ({"foo": "bar"}, list | int),
            ({"foo": "bar"}, SimpleDict | list),
            (["str_val", 1, True], SimpleDict),
            (
                {"str_key": "hello", "int_key": "1", "bool_key": True},
                SimpleDict,
            ),
            ({"str_key": "hello", "int_key": "1"}, SimpleDict),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"bad_key": 1},
                },
                NestedDict,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"int_key": 1},
                    "dict_key": {"foo": (1,)},
                },
                NestedDict,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "list_key": ["a", "b", "c"],
                },
                ListDict,
            ),
            ({"list_key": [1, 2, 3, "hello"]}, ListDict),
            ({"list_key_2": [1, 2, 3.0, "hello"]}, ListDict),
            ({"list_key_3": [1.0]}, ListDict),
            ({"list_key_4": [1.0]}, ListDict),
            ({"list_key_5": [1.0]}, ListDict),
            ({"list_key_6": [1.0]}, ListDict),
            ({"list_key_7": [1.0]}, ListDict),
            ({"list_key_6": [{"foo": 1.0}]}, ListDict),
            ({"list_key_6": [{"foo": None}]}, ListDict),
            ({"list_key_7": [{"foo": {}}]}, ListDict),
            (["str_val", 1, True], SimpleTuple),
            (
                {"str_key": "hello", "int_key": "1", "bool_key": True},
                SimpleDict,
            ),
            ({"str_key": "hello", "int_key": "1"}, SimpleTuple),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"bad_key": 1},
                },
                NestedTuple,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"int_key": 1},
                    "dict_key": {"foo": (1,)},
                },
                NestedTuple,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "list_key": ["a", "b", "c"],
                },
                ListTuple,
            ),
            ({"list_key": [1, 2, 3, "hello"]}, ListTuple),
            ({"list_key_2": [1, 2, 3.0, "hello"]}, ListTuple),
            ({"list_key_3": [1.0]}, ListTuple),
            ({"list_key_4": [1.0]}, ListTuple),
            ({"list_key_5": [1.0]}, ListTuple),
            ({"list_key_6": [1.0]}, ListTuple),
            ({"list_key_7": [1.0]}, ListTuple),
            ({"list_key_6": [{"foo": 1.0}]}, ListTuple),
            ({"list_key_6": [{"foo": None}]}, ListTuple),
            ({"list_key_7": [{"foo": {}}]}, ListTuple),
            (["str_val", 1, True], SimpleTuple),
            (
                {"str_key": "hello", "int_key": "1", "bool_key": True},
                SimpleTuple,
            ),
        ),
    )
    def test_fails_invalid_objects(value: object, schema: Schema) -> None:
        """Success cases."""
        assert not is_json_schema(value, schema)

    @staticmethod
    @pytest.mark.parametrize(
        "value, schema",
        (
            ({"foo": "bar"}, Dict),
            ({"foo": "bar"}, Dict[str, str]),
            ({"foo": ["bar", 1]}, Dict[str, list]),  # type: ignore
            ({"foo": ["bar", 1]}, Dict[str, List]),  # type: ignore
            ({"foo": ["bar"]}, Dict[str, List[str]]),
            ({"foo": {"bar": 1, "baz": "2"}}, Dict[str, dict]),  # type: ignore
            ({"foo": {"bar": 1, "baz": "2"}}, Dict[str, Dict]),  # type: ignore
            ({"foo": {"bar": 1}}, Dict[str, Dict[str, int]]),
            ({"foo": "bar"}, dict | Dict[str, str]),
            ({"foo": "bar"}, Dict[str, str] | dict),
            ({"foo": "bar"}, SimpleDict | Dict[str, str]),
            ({"foo": "bar"}, dict | SimpleDict),
            ({"foo": {"bar": 1}}, SimpleDict | Dict[str, Dict[str, int]]),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, SimpleDict),
            ({"str_key": "hello", "int_key": 1}, SimpleDict),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, NestedDict),
            (
                {"str_key": "hello", "int_key": 1, "bool_key": False, "schema_key": {}},
                NestedDict,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"str_key": "hello"},
                },
                NestedDict,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"str_key": "hello"},
                    "dict_key": {"foo": 1},
                },
                NestedDict,
            ),
            ({"optional_schema_key": {"int_key": 1}}, NestedDict),
            ({"optional_schema_key": None}, NestedDict),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, ListDict),
            (
                {"str_key": "hello", "int_key": 1, "bool_key": False, "list_key": []},
                ListDict,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "list_key": [1, 2, 3],
                },
                ListDict,
            ),
            ({"list_key_2": [1, 2, 3, "hello"]}, ListDict),
            ({"list_key_3": [None, 1, 2, 3, "hello"]}, ListDict),
            ({"list_key_4": [None, 1, 2, 3, "hello"]}, ListDict),
            (
                {"list_key_5": [{"str_key": "hello", "int_key": 1, "bool_key": False}]},
                ListDict,
            ),
            ({"list_key_6": [{"foo": "a", "bar": 1}]}, ListDict),
            ({"list_key_7": [{"foo": True, "bar": None}]}, ListDict),
            ({"list_key_8": [{"foo": True, "bar": None}]}, ListDict),
            ({"list_key_8": [{"foo": {}}]}, ListDict),
            ({"list_key_8": [{"foo": {"bar": 1}}]}, ListDict),
            ({"list_key_8": [{"foo": []}]}, ListDict),
            ({"list_key_8": [{"foo": [{}]}]}, ListDict),
            ({"list_key_8": [{"foo": [{"foo": None}]}]}, ListDict),
            ({"list_key": []}, ListDict),
            ({"list_key_2": []}, ListDict),
            ({"list_key_3": []}, ListDict),
            ({"list_key_4": []}, ListDict),
            ({"list_key_5": []}, ListDict),
            ({"list_key_6": []}, ListDict),
            ({"list_key_7": []}, ListDict),
            ({"list_key_8": []}, ListDict),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, SimpleTuple),
            ({"str_key": "hello", "int_key": 1}, SimpleTuple),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, NestedTuple),
            (
                {"str_key": "hello", "int_key": 1, "bool_key": False, "schema_key": {}},
                NestedTuple,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"str_key": "hello"},
                },
                NestedTuple,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "schema_key": {"str_key": "hello"},
                    "dict_key": {"foo": 1},
                },
                NestedTuple,
            ),
            ({"optional_schema_key": {"int_key": 1}}, NestedTuple),
            ({"optional_schema_key": None}, NestedTuple),
            ({"str_key": "hello", "int_key": 1, "bool_key": False}, ListTuple),
            (
                {"str_key": "hello", "int_key": 1, "bool_key": False, "list_key": []},
                ListTuple,
            ),
            (
                {
                    "str_key": "hello",
                    "int_key": 1,
                    "bool_key": False,
                    "list_key": [1, 2, 3],
                },
                ListTuple,
            ),
            ({"list_key_2": [1, 2, 3, "hello"]}, ListTuple),
            ({"list_key_3": [None, 1, 2, 3, "hello"]}, ListTuple),
            ({"list_key_4": [None, 1, 2, 3, "hello"]}, ListTuple),
            (
                {"list_key_5": [{"str_key": "hello", "int_key": 1, "bool_key": False}]},
                ListTuple,
            ),
            ({"list_key_6": [{"foo": "a", "bar": 1}]}, ListTuple),
            ({"list_key_7": [{"foo": True, "bar": None}]}, ListTuple),
            ({"list_key_8": [{"foo": True, "bar": None}]}, ListDict),
            ({"list_key_8": [{"foo": {}}]}, ListDict),
            ({"list_key_8": [{"foo": {"bar": 1}}]}, ListDict),
            ({"list_key_8": [{"foo": []}]}, ListDict),
            ({"list_key_8": [{"foo": [{}]}]}, ListDict),
            ({"list_key_8": [{"foo": [{"foo": None}]}]}, ListDict),
            ({"list_key": []}, ListTuple),
            ({"list_key_2": []}, ListTuple),
            ({"list_key_3": []}, ListTuple),
            ({"list_key_4": []}, ListTuple),
            ({"list_key_5": []}, ListTuple),
            ({"list_key_6": []}, ListTuple),
            ({"list_key_7": []}, ListTuple),
            ({"list_key_8": []}, ListTuple),
        ),
    )
    def test_passes_valid_objects(value: object, schema: Schema) -> None:
        """Success cases."""
        assert is_json_schema(value, schema)

    @staticmethod
    @pytest.mark.parametrize(
        "value, schema, exp_logs",
        (
            (
                "foo",
                Dict,
                ({"func": "is_json_schema", "args": ("foo", Dict, False)},),
            ),
            (
                {"foo": "bar"},
                List,
                (
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            {"foo": "bar"},
                            List,
                            "_is_json_schema_value_list",
                            False,
                        ),
                    },
                    {"func": "is_json_schema", "args": ({"foo": "bar"}, List, False)},
                ),
            ),
            (
                {"foo": "bar"},
                Dict,
                (
                    {"func": "is_json", "args": ({"foo": "bar"}, True)},
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            {"foo": "bar"},
                            Dict,
                            "_is_json_schema_value_dict",
                            True,
                        ),
                    },
                    {"func": "is_json_schema", "args": ({"foo": "bar"}, Dict, True)},
                ),
            ),
            (
                {"foo": "bar"},
                Dict[str, str],
                (
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            "bar",
                            str,
                            "_is_json_schema_value_simple",
                            True,
                        ),
                    },
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            {"foo": "bar"},
                            Dict[str, str],
                            "_is_json_schema_value_generic_dict",
                            True,
                        ),
                    },
                    {
                        "func": "is_json_schema",
                        "args": ({"foo": "bar"}, Dict[str, str], True),
                    },
                ),
            ),
            (
                {"str_key": "bar"},
                SimpleDict,
                (
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            "bar",
                            str,
                            "_is_json_schema_value_simple",
                            True,
                        ),
                    },
                    {
                        "func": "_is_json_schema_value",
                        "args": (
                            {"str_key": "bar"},
                            SimpleDict,
                            "_is_json_schema_value_annotated",
                            True,
                        ),
                    },
                    {
                        "func": "is_json_schema",
                        "args": ({"str_key": "bar"}, SimpleDict, True),
                    },
                ),
            ),
        ),
    )
    def test_logging(
        value: object,
        schema: Schema,
        exp_logs: Sequence[Mapping[str, object]],
        caplog: pytest.LogCaptureFixture,
    ) -> None:
        """Log messages."""
        caplog.set_level(logging.DEBUG, logger="typeguards.json")
        is_json_schema(value, schema)
        assert len(caplog.records) == len(exp_logs)
        for log_record, exp_log in zip(caplog.records, exp_logs):
            assert log_record.levelname == "DEBUG"
            if log_record.funcName == "is_json":
                assert log_record.msg == "\n\tvalue=%s\n\tres=%s"
            elif log_record.funcName == "is_json_schema":
                assert log_record.msg == "\n\tvalue=%s\n\tschema=%s\n\tres=%s"
            else:
                assert (
                    log_record.msg
                    == "\n\tvalue=%s\n\tschema=%s\n\tcheck_fn=%s\n\tres=%s"
                )
            assert log_record.funcName == exp_log["func"]
            assert log_record.args == exp_log["args"]
