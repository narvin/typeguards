PACKAGE := typeguards
SRC_DIR := src
TEST_DIR := test
CODE_DIRS := $(SRC_DIR) $(TEST_DIR)
DOCS_DIR := docs
DOCS_STATIC_DIR := $(DOCS_DIR)/_static
DOCS_BUILD_DIR := $(DOCS_DIR)/_build/html
INSTALL := pip install -e
FORMAT := python -m black
LINT := python -m pylint -s n
STYLE := python -m pycodestyle
TYPE := python -m mypy --no-error-summary
TEST := python -m pytest -n auto
TEST_ALL := python -m tox --parallel
BUILD := python -m build
GETVERSION := python -m getversions -ie $(PACKAGE)
UPLOAD := python -m twine upload --disable-progress-bar --verbose dist/*
DOCS := sphinx-build -b html
TAGS := ctags -R

.PHONY: all install install-check check format format-check lint style type test \
	test-all build deploy docs tags clean

all: lint style type format-check

install:
	@printf '===== INSTALL =====\n'
	@$(INSTALL) .

install-check:
	@printf '===== INSTALL w/ CHECK EXTRAS =====\n'
	@$(INSTALL) '.[code_quality,docs,test]'

install-all:
	@printf '===== INSTALL w/ ALL EXTRAS =====\n'
	@$(INSTALL) '.[code_quality,docs,test,deploy]'

check: all

format:
	@printf '===== FORMAT =====\n'
	@$(FORMAT) $(CODE_DIRS)

format-check:
	@printf '===== FORMAT CHECK =====\n'
	@$(FORMAT) --check $(CODE_DIRS)

lint:
	@printf '===== LINT =====\n'
	@$(LINT) $(CODE_DIRS)

style:
	@printf '===== STYLE =====\n'
	@$(STYLE) $(CODE_DIRS)

type:
	@printf '===== TYPE =====\n'
	@$(TYPE) $(CODE_DIRS)

test:
	@printf '===== TEST =====\n'
	@$(TEST) $(TEST_DIR)

test-all:
	@printf '===== TEST ALL =====\n'
	@$(TEST_ALL)

build:
	@printf '===== BUILD =====\n'
	@$(BUILD)

deploy:
	@printf '===== DEPLOY =====\n'
	@$(BUILD)
	@$(INSTALL) .
	@version="$$($(GETVERSION))" \
	&& $(UPLOAD) \
	|| printf 'Not deploying, %s %s already in repo.\n' '$(PACKAGE)' "$${version}"

docs:
	@printf '===== DOCS =====\n'
	@mkdir -p $(DOCS_STATIC_DIR)
	@$(DOCS) $(DOCS_DIR) $(DOCS_BUILD_DIR)

tags:
	@printf '===== TAGS =====\n'
	@find . -type d -name site-packages | xargs $(TAGS) $(CODE_DIRS)

clean:
	@printf '===== CLEAN =====\n'
	@find $(CODE_DIRS) -type d -name __pycache__ -exec $(RM) -rf {} \+
