"""A package for static type narrowing, and runtime type checking."""

__version__ = "0.2.0"
